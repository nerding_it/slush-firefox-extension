* slush-firefox-extension
  A [[http://slushjs.github.io/#/][slush]] generator for firefox extension

* Installation
  Install ~slush-firefox-extension~ globally
  #+BEGIN_SRC sh
    npm install -g slush-firefox-extension
  #+END_SRC
  Remember to install ~slush~ globally as well, if you haven't already
  #+BEGIN_SRC sh
    npm install -g slush
  #+END_SRC
  You'll also need to have ~bower~ installed for a smooth installation
  #+BEGIN_SRC sh
    npm install -g bower
  #+END_SRC

* Usage
** Create a new directory for your project
   #+BEGIN_SRC sh
     mkdir my-first-firefox-extension
   #+END_SRC

** Run the generator from within the new directory
   #+BEGIN_SRC sh
     cd my-first-firefox-extension
     slush firefox-extension
   #+END_SRC
   You will now be prompted to give your new firefox extension a name, description & home page
