const gulp = require('gulp'),
      install = require('gulp-install'),
      conflict = require('gulp-conflict'),      
      template = require('gulp-template'),
      inquirer = require('inquirer'),
      _ = require('underscore.string');
    
gulp.task('default', function (done) {
  inquirer.prompt([
    { type: 'input', name: 'name', message: 'What do you want to name your firefox extension?', default: 'my-first-firefox-extension'},
    { type: 'input', name: 'description', message: 'What do you want to add for description?', default: ''},
    { type: 'input', name: 'homePage', message: 'Whats the home page url?', default: ''}
  ], function (answers) {
    answers.nameDashed = _.dasherize(answers.name).replace('-', '');
    answers.modulename = _.camelize(_.slugify(answers.name));
    const files = [__dirname + '/templates/**'];
    return gulp.src(files)
      .pipe(template(answers))
      .pipe(conflict('./'))
      .pipe(gulp.dest('./'))
      .pipe(install())
      .on('finish', function () {
	done()
      })
  })
});	
