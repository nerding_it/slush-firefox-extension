# slush-firefox-extension

A [slush](http://slushjs.github.io/#/) generator for firefox extension

# Installation

Install `slush-firefox-extension` globally

``` {.bash org-language="sh"}
npm install -g slush-firefox-extension
```

Remember to install `slush` globally as well, if you haven\'t already

``` {.bash org-language="sh"}
npm install -g slush
```

You\'ll also need to have `bower` installed for a smooth installation

``` {.bash org-language="sh"}
npm install -g bower
```

# Usage

## Create a new directory for your project

``` {.bash org-language="sh"}
mkdir my-first-firefox-extension
```

## Run the generator from within the new directory

``` {.bash org-language="sh"}
cd my-first-firefox-extension
slush firefox-extension
```

You will now be prompted to give your new firefox extension a name,
description & home page
